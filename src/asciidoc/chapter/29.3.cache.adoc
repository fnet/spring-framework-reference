[[cache-annotations-cacheable-key]]
===== Custom Key Generation Declaration
Since caching is generic, it is quite likely the target methods have various signatures
that cannot be simply mapped on top of the cache structure. This tends to become obvious
when the target method has multiple arguments out of which only some are suitable for
caching (while the rest are used only by the method logic). For example:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	@Cacheable("books")
	public Book findBook(ISBN isbn, boolean checkWarehouse, boolean includeUsed)
----

At first glance, while the two `boolean` arguments influence the way the book is found,
they are no use for the cache. Further more what if only one of the two is important
while the other is not?

For such cases, the `@Cacheable` annotation allows the user to specify how the key is
generated through its `key` attribute. The developer can use <<expressions,SpEL>> to
pick the arguments of interest (or their nested properties), perform operations or even
invoke arbitrary methods without having to write any code or implement any interface.
This is the recommended approach over the
<<cache-annotations-cacheable-default-key,default generator>> since methods tend to be
quite different in signatures as the code base grows; while the default strategy might
work for some methods, it rarely does for all methods.

Below are some examples of various SpEL declarations - if you are not familiar with it,
do yourself a favor and read <<expressions>>:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	@Cacheable(value="books", **key="#isbn"**)
	public Book findBook(ISBN isbn, boolean checkWarehouse, boolean includeUsed)

	@Cacheable(value="books", **key="#isbn.rawNumber"**)
	public Book findBook(ISBN isbn, boolean checkWarehouse, boolean includeUsed)

	@Cacheable(value="books", **key="T(someType).hash(#isbn)"**)
	public Book findBook(ISBN isbn, boolean checkWarehouse, boolean includeUsed)
----

The snippets above show how easy it is to select a certain argument, one of its
properties or even an arbitrary (static) method.

If the algorithm responsible to generate the key is too specific or if it needs
to be shared, you may define a custom `keyGenerator` on the operation. To do
this, specify the name of the `KeyGenerator` bean implementation to use:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	@Cacheable(value="books", **keyGenerator="myKeyGenerator"**)
	public Book findBook(ISBN isbn, boolean checkWarehouse, boolean includeUsed)
----


[NOTE]
====
The `key` and `keyGenerator` parameters are mutually exclusive and an operation
specifying both will result in an exception.
====

[[cache-annotations-cacheable-default-cache-resolver]]
===== Default Cache Resolution

Out of the box, the caching abstraction uses a simple `CacheResolver` that
retrieves the cache(s) defined at the operation level using the configured
`CacheManager`.

To provide a different __default__ cache resolver, one needs to implement the
`org.springframework.cache.interceptor.CacheResolver` interface.


