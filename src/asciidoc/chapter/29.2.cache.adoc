[[cache-annotations]]
=== Declarative annotation-based caching
For caching declaration, the abstraction provides a set of Java annotations:

* `@Cacheable` triggers cache population
* `@CacheEvict` triggers cache eviction
* `@CachePut` updates the cache without interfering with the method execution
* `@Caching` regroups multiple cache operations to be applied on a method
* `@CacheConfig` shares some common cache-related settings at class-level

Let us take a closer look at each annotation:

[[cache-annotations-cacheable]]
==== @Cacheable annotation

As the name implies, `@Cacheable` is used to demarcate methods that are cacheable - that
is, methods for whom the result is stored into the cache so on subsequent invocations
(with the same arguments), the value in the cache is returned without having to actually
execute the method. In its simplest form, the annotation declaration requires the name
of the cache associated with the annotated method:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	@Cacheable("books")
	public Book findBook(ISBN isbn) {...}
----

In the snippet above, the method `findBook` is associated with the cache named `books`.
Each time the method is called, the cache is checked to see whether the invocation has
been already executed and does not have to be repeated. While in most cases, only one
cache is declared, the annotation allows multiple names to be specified so that more
than one cache are being used. In this case, each of the caches will be checked before
executing the method - if at least one cache is hit, then the associated value will be
returned:

[NOTE]
====
All the other caches that do not contain the value will be updated as well even though
the cached method was not actually executed.
====

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	@Cacheable({"books", "isbns"})
	public Book findBook(ISBN isbn) {...}
----


[[cache-annotations-cacheable-default-key]]
===== Default Key Generation
Since caches are essentially key-value stores, each invocation of a cached method needs
to be translated into a suitable key for cache access. Out of the box, the caching
abstraction uses a simple `KeyGenerator` based on the following algorithm:

* If no params are given, return `SimpleKey.EMPTY`.
* If only one param is given, return that instance.
* If more the one param is given, return a `SimpleKey` containing all parameters.

This approach works well for most use-cases; As long as parameters have __natural keys__
and implement valid `hashCode()` and `equals()` methods. If that is not the case then the
strategy needs to be changed.

To provide a different __default__ key generator, one needs to implement the
`org.springframework.cache.interceptor.KeyGenerator` interface.


[NOTE]
====
The default key generation strategy changed with the release of Spring 4.0. Earlier
versions of Spring used a key generation strategy that, for multiple key parameters,
only considered the `hashCode()` of parameters and not `equals()`; this could cause
unexpected key collisions (see https://jira.spring.io/browse/SPR-10237[SPR-10237]
for background). The new 'SimpleKeyGenerator' uses a compound key for such scenarios.

If you want to keep using the previous key strategy, you can configure the deprecated
`org.springframework.cache.interceptor.DefaultKeyGenerator` class or create a custom
hash-based 'KeyGenerator' implementation.
====


